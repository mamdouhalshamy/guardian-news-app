package com.innozones.mms.guardiannewsapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.net.URL;

import static junit.framework.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class NetworkUtilsTest {

    @Test
    public void buildBasicUrl() {
        String keyword = "Google";
        String expectedUrl = "https://content.guardianapis.com/search?q=Google&api-key=bb4e3dbf-edb2-4b3c-9515-049c871e823e";
        URL url = NetworkUtils.buildGuardianQueryUrl(keyword);
        String actualUrl = url.toString();
        assertEquals(expectedUrl, actualUrl);
    }
}