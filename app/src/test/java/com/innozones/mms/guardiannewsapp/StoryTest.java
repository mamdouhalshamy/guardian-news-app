package com.innozones.mms.guardiannewsapp;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class StoryTest {

    @Test
    public void checkStory(){
        String title = "Debate over NME’s heyday | Letters";
        String section = "Music";
        String url = "https://www.theguardian.com/music/2018/mar/09/debate-over-nmes-heyday";
        String datePublished = "2018-03-09T17:05:42Z";
        String authorName = "nns";
        Story story;
        story = new Story(title, section, url, datePublished, authorName);

        assertEquals(title , story.getTitle());
        assertEquals(section, story.getSection());
        assertEquals(url, story.getUrl());
        assertEquals(datePublished, story.getDatePublished());
        assertEquals(authorName, story.getAuthorName());
        assertTrue(story.hasAuthor());
        assertTrue(story.hasDatePublished());
    }


    @Test
    public void checkStoryWithoutAuthor(){
        String title = "Debate over NME’s heyday | Letters";
        String section = "Music";
        String url = "https://www.theguardian.com/music/2018/mar/09/debate-over-nmes-heyday";
        String datePublished = "2018-03-09T17:05:42Z";
        Story story;
        story = new Story(title, section, url, datePublished);

        assertEquals(title , story.getTitle());
        assertEquals(section, story.getSection());
        assertEquals(url, story.getUrl());
        assertEquals(datePublished, story.getDatePublished());
        assertEquals(null, story.getAuthorName());
        assertFalse(story.hasAuthor());
    }

    @Test
    public void checkStoryPrimitive(){
        String title = "Debate over NME’s heyday | Letters";
        String section = "Music";
        String url = "https://www.theguardian.com/music/2018/mar/09/debate-over-nmes-heyday";
        Story story;
        story = new Story(title, section, url);

        assertEquals(title , story.getTitle());
        assertEquals(section, story.getSection());
        assertEquals(url, story.getUrl());
        assertEquals(null, story.getDatePublished());
        assertEquals(null, story.getAuthorName());
        assertFalse(story.hasAuthor());
        assertFalse(story.hasDatePublished());
    }



}