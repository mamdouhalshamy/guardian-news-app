package com.innozones.mms.guardiannewsapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityRecyclerViewTest {


    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void makeSureThatRecyclerViewIsExist() {
        // make search
        onView(withId(R.id.keyword_edit_text))
                .perform(typeText("google"));
        onView(withId(R.id.search_button))
                .perform(click());

        onView(withId(R.id.results_recycler_view))
                .check(matches(hasDescendant(isDisplayed())));
    }
}
