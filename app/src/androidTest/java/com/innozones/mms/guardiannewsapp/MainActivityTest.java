package com.innozones.mms.guardiannewsapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void userDidNotEnterKeyword_clickedSearch_emptyViewShouldEncourageUserToEnterKeyword() {
        onView(withId(R.id.search_button))
                .perform(click());
        onView(withId(R.id.empty_view_text))
                .check(matches(withText(R.string.no_search_request)));
    }

}