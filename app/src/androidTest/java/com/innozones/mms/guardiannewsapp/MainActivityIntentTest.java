package com.innozones.mms.guardiannewsapp;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class MainActivityIntentTest {

    @Rule
    public IntentsTestRule<MainActivity> mActivityRule = new IntentsTestRule<>(
            MainActivity.class);

    @Test
    public void userClickOnListItem_BrowserShowStoryDetails() {
        // make search
        onView(withId(R.id.keyword_edit_text))
                .perform(typeText("google"));
        onView(withId(R.id.search_button))
                .perform(click());

        onView(withId(R.id.results_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));


        intended(allOf(
                hasAction(Intent.ACTION_VIEW),
                toPackage("com.android.chrome")));


    }
}
