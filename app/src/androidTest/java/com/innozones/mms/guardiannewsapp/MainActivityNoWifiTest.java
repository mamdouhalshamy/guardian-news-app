package com.innozones.mms.guardiannewsapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertFalse;

@RunWith(AndroidJUnit4.class)
public class MainActivityNoWifiTest {

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    Context context;

    @Before
    public void closeWifi() {
        setWifiState(false);
    }

    @Test
    public void noInternetConnection_emptyViewTextShouldShowNoInternet() {
        assertFalse(isConnected(context));
        onView(withId(R.id.keyword_edit_text))
                .perform(typeText("Google"));
        onView(withId(R.id.search_button))
                .perform(click());
        onView(withId(R.id.empty_view_text))
                .check(matches(withText(R.string.no_internet_connection)));
    }


    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @After
    public void openWifi() {
        setWifiState(true);
    }

    public void setWifiState(boolean state) {
        context = activityActivityTestRule.getActivity();

        WifiManager wifiManager = (WifiManager) activityActivityTestRule
                .getActivity().getSystemService(Context.WIFI_SERVICE);
        boolean isWifiStated = false;
        while (!isWifiStated)
            isWifiStated = wifiManager.setWifiEnabled(state);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
