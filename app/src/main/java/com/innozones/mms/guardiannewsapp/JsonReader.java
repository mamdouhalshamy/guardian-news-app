package com.innozones.mms.guardiannewsapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonReader {
    private static final String LOG_TAG = "JsonReader";

    private static final String RESPONSE = "response";
    private static final String RESULTS = "results";
    private static final String TITLE = "webTitle";
    private static final String SECTION = "sectionName";
    private static final String URL = "webUrl";
    private static final String PUBLICATION_DATE = "webPublicationDate";
    private static final String TAGS = "tags";
    private static final String MESSAGE = "message";

    static final String AUTHENTICATION_ERROR_MESSAGE = "Invalid authentication credentials";
    static final String API_LIMIT_EXCEEDED_ERROR_MESSAGE = "API rate limit exceeded";

    // FIXME better way to handle errors
    static final int NO_ERROR = 0;
    static final int CORRUPTED_JSON = 10;
    static final int API_RATE_LIMIT_EXCEEDED_ERROR = 100;
    static final int AUTHENTICATION_ERROR = 500;
    static final int EMPTY_RESULT_ERROR = 700;
    static final int UNHANDLED_ERROR = 1000;

    static int errorCode = NO_ERROR;


    static ArrayList<Story> getStories(String json) {
        errorCode = NO_ERROR;
        ArrayList<Story> stories = new ArrayList<>();
        JSONObject main = null;
        try {
            main = new JSONObject(json);
        } catch (JSONException e) {
            errorCode = CORRUPTED_JSON;
            e.printStackTrace();
        }

        if (main.has(RESPONSE)) {
            JSONObject response = main.optJSONObject(RESPONSE);
            JSONArray storiesArray = response.optJSONArray(RESULTS);
            if (storiesArray.length() == 0)
                errorCode = EMPTY_RESULT_ERROR;
            else {
                for (int i = 0; i < storiesArray.length(); i++) {
                    JSONObject storyJson = storiesArray.optJSONObject(i);
                    String title = storyJson.optString(TITLE);
                    String section = storyJson.optString(SECTION);
                    String url = storyJson.optString(URL);

                    String authorName = null;
                    if (storyJson.has(TAGS)) {
                        JSONArray arr = storyJson.optJSONArray(TAGS);
                        if (!arr.isNull(0)) {
                            JSONObject authorData = arr.optJSONObject(0);
                            authorName = authorData.optString(TITLE);
                        }
                    }

                    String publicationDate = null;
                    if (storyJson.has(PUBLICATION_DATE))
                        publicationDate = storyJson.optString(PUBLICATION_DATE);

                    stories.add(new Story(title, section, url, publicationDate, authorName));
                }
            }
        } else if (main.has(MESSAGE)) {
            String message = main.optString(MESSAGE);

            switch (message) {
                case AUTHENTICATION_ERROR_MESSAGE:
                    errorCode = AUTHENTICATION_ERROR;
                    break;

                case API_LIMIT_EXCEEDED_ERROR_MESSAGE:
                    errorCode = API_RATE_LIMIT_EXCEEDED_ERROR;
                    break;
                default:
                    errorCode = UNHANDLED_ERROR;
            }
        } else {
            errorCode = UNHANDLED_ERROR; // Unhandled Case
        }
        return stories;
    }


    static int getErrorCode() {
        return errorCode;
    }
}


