package com.innozones.mms.guardiannewsapp;

public class Story {
    /**
     * This method is used to set Story parameters
     * @param title Title of the Story
     * @param section Section of the Story
     * @param url Url of the Story
     * @param datePublished Published date of the Story
     * @param authorName the Author of the Story
     */
    public Story(String title, String section, String url, String datePublished, String authorName){
        this.title = title;
        this.section = section;
        this.url = url;
        this.datePublished = datePublished;
        this.authorName = authorName;
    }

    /**
     * This method is used to set Story parameters
     * @param title Title of the Story
     * @param section Section of the Story
     * @param url Url of the Story
     */
    public Story(String title, String section, String url){
        this.title = title;
        this.section = section;
        this.url = url;
    }

    /**
     * This method is used to set Story parameters
     * @param title Title of the Story
     * @param section Section of the Story
     * @param url Url of the Story
     * @param datePublished Published date of the Story
     */
    public Story(String title, String section, String url, String datePublished){
        this.title = title;
        this.section = section;
        this.url = url;
        this.datePublished = datePublished;
    }

    private String title;
    private String section;
    private String authorName;
    private String datePublished;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean hasAuthor(){
        if(this.authorName != null)
            return true;
        return false;
    }

    public boolean hasDatePublished(){
        if(this.datePublished != null)
            return true;
        return false;
    }
}
