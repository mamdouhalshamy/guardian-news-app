package com.innozones.mms.guardiannewsapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class StoryViewHolder extends RecyclerView.ViewHolder {
    public final TextView titleTextView;
    public final TextView sectionTextView;
    public String url = "";

    public StoryViewHolder(View itemView, final Context context) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.title_text_view);
        sectionTextView = itemView.findViewById(R.id.section_text_view);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri storyUri = Uri.parse(url);
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, storyUri);
                context.startActivity(websiteIntent);
            }
        });
    }
}
