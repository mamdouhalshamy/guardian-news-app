package com.innozones.mms.guardiannewsapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class StoryAdapter extends RecyclerView.Adapter<StoryViewHolder> {
    private List<Story> mStories;
    private Context mContext;

    public StoryAdapter(List<Story> stories, Context context) {
        mStories = stories;
        mContext = context;
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item, parent, false);
        return new StoryViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryViewHolder holder, int position) {
        final Story story = mStories.get(position);
        holder.titleTextView.setText(story.getTitle());
        holder.sectionTextView.setText(story.getSection());
        holder.url = story.getUrl();
    }

    @Override
    public int getItemCount() {
        return mStories.size();
    }

    public void addAll(ArrayList<Story> stories) {
        mStories = stories;
    }
}
