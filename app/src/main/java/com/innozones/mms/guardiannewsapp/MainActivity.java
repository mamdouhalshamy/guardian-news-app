package com.innozones.mms.guardiannewsapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.innozones.mms.guardiannewsapp.util.EspressoIdlingResource;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<ArrayList<Story>> {

    @Nullable
    private EspressoIdlingResource mIdlingResource;

    Button searchButton;
    EditText keywordEditText;
    EmptyRecyclerView resultsRecyclerView;
    LinearLayout emptyView;
    TextView emptyViewText;
    ImageView emptyViewImage;
    ProgressBar loadingProgressBar;

    ArrayList<Story> stories;
    private static final int GUARDIAN_SEARCH_LOADER = 23;
    private static final String SEARCH_QUERY_URL_EXTRA = "query";

    StoryAdapter mStoryAdapter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the IdlingResource instance
        getIdlingResource();


        searchButton = findViewById(R.id.search_button);
        keywordEditText = findViewById(R.id.keyword_edit_text);
        resultsRecyclerView = findViewById(R.id.results_recycler_view);
        emptyView = findViewById(R.id.empty_view);
        emptyViewText = findViewById(R.id.empty_view_text);
        emptyViewImage = findViewById(R.id.empty_view_image);

        loadingProgressBar = findViewById(R.id.loading_progress_bar);

        stories = new ArrayList<>();

        context = this;

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = keywordEditText.getText().toString();
                if (keyword.isEmpty()) {
                    emptyViewImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_24dp));
                    emptyViewText.setText(getString(R.string.no_search_request));
                    Toast.makeText(view.getContext(), getString(R.string.no_search_request), Toast.LENGTH_LONG).show();
                } else {
                    ConnectivityManager cm =
                            (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                    if (isConnected) {
                        loadingProgressBar.setVisibility(VISIBLE);
                        startStoriesAsyncTaskLoader(keyword);
                    } else {
                        emptyViewImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_signal_wifi_24dp));
                        emptyViewText.setText(getString(R.string.no_internet_connection));
                        Toast.makeText(view.getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        resultsRecyclerView.setHasFixedSize(true);
        resultsRecyclerView.setLayoutManager(layoutManager);
        mStoryAdapter = new StoryAdapter(stories, this);
        resultsRecyclerView.setAdapter(mStoryAdapter);
        resultsRecyclerView.setEmptyView(emptyView);

    }

    private void startStoriesAsyncTaskLoader(String keyword) {
        // The IdlingResource is null in production.
        if (mIdlingResource != null)
            mIdlingResource.setIdleState(false);

        URL guardianSearchUrl = NetworkUtils.buildGuardianQueryUrl(keyword);

        Bundle queryBundle = new Bundle();
        queryBundle.putString(SEARCH_QUERY_URL_EXTRA, guardianSearchUrl.toString());

        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<ArrayList<Story>> guardianSearchLoader = loaderManager.getLoader(GUARDIAN_SEARCH_LOADER);

        if (guardianSearchLoader == null) {
            loaderManager.initLoader(GUARDIAN_SEARCH_LOADER, queryBundle, this);
        } else {
            loaderManager.restartLoader(GUARDIAN_SEARCH_LOADER, queryBundle, this);
        }
    }

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new EspressoIdlingResource();
        }
        return mIdlingResource;
    }


    @NonNull
    @Override
    public Loader<ArrayList<Story>> onCreateLoader(int id, @Nullable final Bundle args) {
        return new AsyncTaskLoader<ArrayList<Story>>(this) {

            ArrayList<Story> mGuardianStories;

            @Override
            protected void onStartLoading() {
                if (args == null)
                    return;

                if (mGuardianStories != null)

                    deliverResult(mGuardianStories);
                else

                    forceLoad();
            }

            @Nullable
            @Override
            public ArrayList<Story> loadInBackground() {
                /* Extract the search query from the args using our constant */
                String searchQueryUrlString = args.getString(SEARCH_QUERY_URL_EXTRA);

                /* If the user didn't enter anything, there's nothing to search for */
                if (searchQueryUrlString == null || TextUtils.isEmpty(searchQueryUrlString)) {
                    return null;
                }

                /* Parse the URL from the passed in String and perform the search */
                try {
                    URL guardianUrl = new URL(searchQueryUrlString);
                    ArrayList<Story> guardianSearchResults = NetworkUtils.getResponseFromHttpUrl(guardianUrl);
                    return guardianSearchResults;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public void deliverResult(ArrayList<Story> guardianStories) {
                mGuardianStories = guardianStories;
                super.deliverResult(mGuardianStories);
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<Story>> loader, ArrayList<Story> stories) {
        loadingProgressBar.setVisibility(GONE);
        emptyViewImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_24dp));
        emptyViewText.setText(getResources().getString(R.string.no_data_available));
        mStoryAdapter.notifyDataSetChanged();
        if (stories != null || !stories.isEmpty()) {
            mStoryAdapter.addAll(stories);
            mStoryAdapter.notifyDataSetChanged();
        }
        // testing
        if (mIdlingResource != null)
            mIdlingResource.setIdleState(true);

    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<Story>> loader) {
        mStoryAdapter.notifyDataSetChanged();
    }


}
